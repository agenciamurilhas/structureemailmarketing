// structureEmailMarketing v1.1.2

//injeta o jQuery
head = document.querySelector('head');
script = document.createElement('script');
script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js';
head.appendChild(script);

//FileSaver.min.js NÃO REMOVER ESTA LINHA DO TOPO
var saveAs=saveAs||function(e){"use strict";if(typeof e==="undefined"||typeof navigator!=="undefined"&&/MSIE [1-9]\./.test(navigator.userAgent)){return}var t=e.document,n=function(){return e.URL||e.webkitURL||e},r=t.createElementNS("http://www.w3.org/1999/xhtml","a"),o="download"in r,a=function(e){var t=new MouseEvent("click");e.dispatchEvent(t)},i=/constructor/i.test(e.HTMLElement)||e.safari,f=/CriOS\/[\d]+/.test(navigator.userAgent),u=function(t){(e.setImmediate||e.setTimeout)(function(){throw t},0)},s="application/octet-stream",d=1e3*40,c=function(e){var t=function(){if(typeof e==="string"){n().revokeObjectURL(e)}else{e.remove()}};setTimeout(t,d)},l=function(e,t,n){t=[].concat(t);var r=t.length;while(r--){var o=e["on"+t[r]];if(typeof o==="function"){try{o.call(e,n||e)}catch(a){u(a)}}}},p=function(e){if(/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(e.type)){return new Blob([String.fromCharCode(65279),e],{type:e.type})}return e},v=function(t,u,d){if(!d){t=p(t)}var v=this,w=t.type,m=w===s,y,h=function(){l(v,"writestart progress write writeend".split(" "))},S=function(){if((f||m&&i)&&e.FileReader){var r=new FileReader;r.onloadend=function(){var t=f?r.result:r.result.replace(/^data:[^;]*;/,"data:attachment/file;");var n=e.open(t,"_blank");if(!n)e.location.href=t;t=undefined;v.readyState=v.DONE;h()};r.readAsDataURL(t);v.readyState=v.INIT;return}if(!y){y=n().createObjectURL(t)}if(m){e.location.href=y}else{var o=e.open(y,"_blank");if(!o){e.location.href=y}}v.readyState=v.DONE;h();c(y)};v.readyState=v.INIT;if(o){y=n().createObjectURL(t);setTimeout(function(){r.href=y;r.download=u;a(r);h();c(y);v.readyState=v.DONE});return}S()},w=v.prototype,m=function(e,t,n){return new v(e,t||e.name||"download",n)};if(typeof navigator!=="undefined"&&navigator.msSaveOrOpenBlob){return function(e,t,n){t=t||e.name||"download";if(!n){e=p(e)}return navigator.msSaveOrOpenBlob(e,t)}}w.abort=function(){};w.readyState=w.INIT=0;w.WRITING=1;w.DONE=2;w.error=w.onwritestart=w.onprogress=w.onwrite=w.onabort=w.onerror=w.onwriteend=null;return m}(typeof self!=="undefined"&&self||typeof window!=="undefined"&&window||this.content);if(typeof module!=="undefined"&&module.exports){module.exports.saveAs=saveAs}else if(typeof define!=="undefined"&&define!==null&&define.amd!==null){define("FileSaver.js",function(){return saveAs})}

function randomString() {
	let randomValues = 'abcdefghijklmnopqrstuvwxyz';
	let randomResult = '';
	for (let i = 0; i < 12; i++) {
		randomResult += randomValues[Math.floor(Math.random() * randomValues.length)];
	};
	return randomResult;
};

jQueryReady = setInterval(function() {
	if( jQuery !== undefined && $ === jQuery ){
		clearInterval(jQueryReady);
		$(function() {
			//pergunta antes de sair da aba do chrome
			window.onbeforeunload = function(event){
				event = event || window.event;
				return 'Sure?';
			}

			//PARTE 1 - ESTRUTURA DAS TABELAS

			jsonImgs = imgsToJSON();
			jsonImgsOriginal = jsonImgs.clone();
			console.log('jsonImgsOriginal', jsonImgsOriginal.clone());

			//style temporário, ele é removido antes do arquivo final
			$('<style>*{margin:0; padding:0; border:0; outline:none;}</style>').appendTo( head );

			//reseta o body (incluindo os attrs)
			$('body').remove();
			$('html').append( $('<body></body>') );

			//inicia a primeira tabela
			table = createTable(autoSetWidth(jsonImgs), 'auto');
			$('body').append(table);
			table.attr({
				'align' : 'center',
				//o idforbackup é um código único, que impede do CTRL + Z ou CTRL + Y se confundir na timeline (tableBackup)
				'idforbackup' : randomString()
			});
			constructive(jsonImgs, table);

			//CTRL + Z e CTRL + Y
			ctrlZ();
			monitoraTableMaster();
			top.tableBackup = [];

			function constructive(jsonImgs, tableMaster) {
				//console.log('jsonImgs', jsonImgs.clone());
				// T1 - define quantas linhas (trs) serão criadas nesta tabela
				let rowImages = [];
				let imgsGroup = [];
				jsonImgs = sortJsonsAB(jsonImgs, 'xOrder');
				xMin = jsonImgs[0].x;
				for (let i = 0; i < jsonImgs.length; i++) {
					if (jsonImgs[i].x == xMin) {
						rowImages.push(jsonImgs[i]);
						jsonImgs[i] = undefined;
						imgsGroup[i] = [];
					};
				};
				// console.log('jsonImgs', jsonImgs.clone());
				// console.log('rowImages', rowImages.clone());
				// console.log('imgsGroup', imgsGroup.clone());

				//T2 identifica quais outras imagens farão parte da mesma linha que as linhas já definidas
				for (let i = 0; i < rowImages.length; i++) {
					if (typeof rowImages[i + 1] != 'undefined') {
						idMax = rowImages[i + 1].id;
					} else {
						idMax = 999999;
					};
					for (let k = 0; k < jsonImgs.length; k++) {
						if (jsonImgs[k] != undefined) {
							if (jsonImgs[k].id < idMax) {
								imgsGroup[i].push(jsonImgs[k]);
								jsonImgs[k] = undefined;
							};
						};
					};
				};
				rowImages = sortJsonsAB(rowImages, 'id');
				for (let i = 0; i < imgsGroup.length; i++) {
					imgsGroup[i] = sortJsonsAB(imgsGroup[i], 'id');
				};
				// console.log('jsonImgs', jsonImgs.clone());
				// console.log('rowImages', rowImages.clone());
				// console.log('imgsGroup', imgsGroup.clone());
				for (let i = 0; i < rowImages.length; i++) {
					loopPerLine(rowImages[i], imgsGroup[i], tableMaster);
				};

				function loopPerLine(thisRowImage, thisImgGroup, tableMaster) {
					thisImgGroup = thisImgGroup.clone();

					//T3 adiciona mais um nível na tabela para torná-la 'profunda'
					//agrupa as imagens para calcular o tamanho total da tabela
					groupSize = getGroupSize(thisRowImage, thisImgGroup);
					//console.log('groupSize', groupSize);
					//TABLE > TBODY
					tableMaster.children().append(
						//TR > TD > TABLE > TBODY > TR > TD
						createTr(groupSize.x, groupSize.y).append(
							createTd(groupSize.x, groupSize.y).append(
								createTable(groupSize.x, groupSize.y).append(
									createTr(groupSize.x, groupSize.y).append(
										td = createTd(groupSize.x, groupSize.y)
									)
								)
							)
						)
					);

					//cria a tabela onda ficará as imagens ou se iniciará o recursivo
					//TABLE > TBODY > TR
					let newTable = createTable(groupSize.x, groupSize.y).append(
						createTr(groupSize.x, groupSize.y)
					);

					//adiciona a tabela nova dentro da tabela 'profunda'
					td.append(newTable);

					//T4 - adiciona as imagens de mesmo tamanho
					//para a thisRowImage também ser adicionada, inserimos ela dentro do grupo
					thisImgGroup.unshift(thisRowImage);
					for (let i = 0; i < thisImgGroup.length; i++) {
						//console.log('loop');
						//se a imagem for do mesmo tamanho da linha
						if (typeof thisImgGroup[i] != 'undefined') {
							if (thisImgGroup[i].height == thisRowImage.height) {

								//TABLE > TBODY > TR
								newTable.children().children().append(
									//adicione a imagem na tabela
									//TD > A (se existir ahref) > IMG
									createTd(autoSetWidth(thisImgGroup[i]), autoSetHeight(thisImgGroup[i])).append(
										createImg(thisImgGroup[i])
									)
								);

								//e remove ela do grupo
								thisImgGroup[i] = undefined;

							} else {
								//senão, precisamos identificar se é possível criar um bloco com alguns itens
								//ou se é necessário criar um bloco com TODOS os itens restantes
								//a partir da imagem atual thisImgGroup[i] calcule quais imagens se iniciam e terminam embaixo
								//para identificar se é possível criar um bloco com elas
								imgsBelow = [];
								for (let k = 0; k < thisImgGroup.length; k++) {
									//se a imagem existe
									if (thisImgGroup[k] != undefined) {
										//se a imagem se INICIA abaixo da imagem
										if (thisImgGroup[k].x < thisImgGroup[i].xLast) {
											imgsBelow.push(thisImgGroup[k]);
										};
									};
								};
								//console.log( 'imgsBelow.clone()', imgsBelow.clone() );
								//verifica se todas as imagens que se INICIAM embaixo também TERMINAM embaixo da thisImgGroup[i]
								//aqui é usado o macete da ordenação tornando desnecessário outro loop como acima
								//como a própria imagem está neste array, para testarmos devemos primeiro copiar o array,
								//remover ela e testar só as outras imagens
								imgsBelowCopy = imgsBelow.clone();
								imgsBelowCopy = sortJsonsAB(imgsBelowCopy, 'id');
								//remove a própria imagem
								imgsBelowCopy[0] = undefined;
								//então ordena para fazer o teste
								imgsBelowCopy = sortJsonsAB(imgsBelowCopy, 'xOrder');
								//se todas as imagens embaixo desta acabam no máximo junto com ela (thisImgGroup[i])
								if (imgsBelowCopy[0].xLast <= thisImgGroup[i].xLast) {
									//então é possível formar uma tabela menor, sem necessidade de usar o recursivo em TODAS
									//as imagens criam uma tabela com os itens que compõem o bloco menor
									//para não enviarmos undefined no recursivo, primeiro limpamos o grupo
									//e faz a ordenação por ID só por garantia
									recursiveGroup = [];
									for (let k = 0; k < imgsBelow.length; k++) {
										if (imgsBelow[k] != undefined) {
											recursiveGroup.push(imgsBelow[k]);
										};
									};
									recursiveGroup = sortJsonsAB(recursiveGroup, 'id');
									//agora precisamos remover as imagens que pegamos aqui do principal
									thisImgGroup = jsonImgsFindAndSetUndefined(thisImgGroup, recursiveGroup);
									//calcula o tamanho da tabela recursiva PEQUENA

									//na tableMaster crie uma TD e adicione uma TABLE que será a recursiva
									groupSize = getGroupSize(recursiveGroup);
									//console.log('groupSize', groupSize);
									//TABLE > TBODY > TR, já presente no DOM
									newTable.children().children().append(
										//cria TD > TABLE > TBODY > TR > TD
										createTd(groupSize.x, groupSize.y).append(
											recursiveTable = createTable(groupSize.x, groupSize.y)
										)
									);
									//console.log('recursivo de bloco MENOR iniciado');
									constructive(recursiveGroup, recursiveTable);

								} else {
									//senão é preciso criar uma tabela e usar o recursivo em TUDO
									//para não enviarmos undefined no recursivo, primeiro limpamos o grupo
									recursiveGroup = [];
									for (let k = 0; k < thisImgGroup.length; k++) {
										if (thisImgGroup[k] != undefined) {
											recursiveGroup.push(thisImgGroup[k]);
											thisImgGroup[k] == undefined;
										};
									};
									//e faz a ordenação por ID só por garantia
									recursiveGroup = sortJsonsAB(recursiveGroup, 'id');



									//na tableMaster crie uma TD e adicione uma TABLE que será a recursiva
									//calcula o tamanho da tabela recursiva
									groupSize = getGroupSize(recursiveGroup);
									//console.log('groupSize', groupSize);
									//TABLE > TBODY > TR, já presente no DOM
									newTable.children().children().append(
										//cria TD > TABLE > TBODY > TR > TD
										createTd(groupSize.x, groupSize.y).append(
											recursiveTable = createTable(groupSize.x, groupSize.y)
										)
									);
									//console.log('recursivo de bloco MAIOR iniciado');
									constructive(recursiveGroup, recursiveTable);

									//remove todos os itens do array principal
									for (let k = 0; k < thisImgGroup.length; k++) {
										thisImgGroup[k] = undefined;
									};
								};
							};
						};
					};	//FOR
				};// /loopPerLine
			};


			//eventos do mouse
			top.eventoClick = function( elementos ){
				//FUNÇÕES DO MOUSE
				elementos.on('click', function(){
					let thisImg = $(this);
					let thisParent = thisImg.parent();
					let mouseAction = $('select').val();

					switch(mouseAction){
						case 'addText':
							//se os campos estiverem preenchidos
							if( $('.psCss').val().length > 0 && $('.psText').val().length > 0 ){
								//se o pai não for um A
								if( thisParent.prop('tagName') != 'A' ){
									//cria o span com o estilo da fonte
									let thisSpan = clickToPsCss( $('.psCss').val(), $('.psText').val() );

									thisParent.append( thisSpan );

									thisParent.attr({
										'style': 'font-size: 0; text-align: ' + span_getHorizontalAlign(event) + ';',
										'valign': span_getVerticalAlign(event),
										'bgcolor': getBgColor(1),
									});

									thisImg.remove();
								}else{
									alert('não é possível criar um texto dentro de um link. Remova o link (elemento A) desta imagem antes disso.');
								};
							}else{
								alert('Os campos de textos não podem estar vazios.');
							};
						break;

						case 'deleteImg':


							//se a imagem contiver link, então remove ele também
							if( thisImg.parent().prop('tagName') == 'A' ){
								//faz a correção, o parent sempre deve ser a TD
								thisParent = thisParent.parent();
								//remove o A
								thisImg.parent().remove();
							}else{
								thisImg.remove();
							};

							thisParent.attr({
								'bgcolor': getBgColor()
							});
						break;

						case 'modifyAltTitle':
							top.imgChangeLink = thisImg;
							$('.lightboxMaster, .box-altTitle').css('display', 'block');
						break;

						case 'addOrModifyLink':
							top.imgAddLink = thisImg;
							$('.lightboxMaster, .box-addOrModifyLink').css('display', 'block');
						break;

						case 'removeA':
							removeA( thisImg, thisParent );
							function removeA( thisImg, thisParent ){
								if( thisParent.prop('tagName') == 'A' ){
									thisImg.insertAfter( thisParent );
									thisParent.remove();
									thisParent.on('click', function(event){
										event.preventDefault();
									});
								};
							};
						break;
					};
				});

			};

			eventoClick($('table').eq(0).find('img'));
			preventA( $('table').eq(0).find('a') );

			function preventA( elementos ){
				//impede que os links funcionem
				elementos.on('click', function(){
					event.preventDefault();
				});
			};



			$('<style>body>table{background:#0000ff;}img:hover{opacity:0.2;}</style>').appendTo('head');

			// GET CSS FONTS
			let lateralBox = $('<div style="position: fixed; top: 0; left: 0;">' + '<div id="box"><div class="container"><textarea class="psCss" placeholder="PHOTOSHOP CSS"></textarea></div><div class="container"><textarea class="psText" placeholder="TEXTO"></textarea></div><div class="container"><textarea class="psBackground" placeholder="BACKGROUND">00ff00</textarea></div><div class="container"><p>Função do mouse:</p><select><option value="deleteImg">Deletar imagem</option><option value="addText">Adicionar texto</option><option value="modifyAltTitle">Alterar atributo Alt e Title</option><option value="addOrModifyLink">Adicionar ou modificar link</option><option value="removeA">Remover link</option></select></div><div class="container"><button class="btnSalvar fixedButtons" onclick="cssBySelection();">Inserir CSS na Seleção</button></div><div class="container"><button class="btnAddCss fixedButtons" onclick="salvar();">Salvar e-mail</button><button class="btnAddCss fixedButtons" onclick="removeHeights();">Remover [Height] desnecessários</button></div></div><div class="lightboxMaster"><div class="lightbox box-altTitle" style="display: none;"><div class="closeLightBox"><p class="closeLightBoxButton" onclick="closeLightBox()">X</p></div><div class="container"><input class="imgAlt" placeholder="ALT e TITLE da imagem"></div><div class="container"><button class="fixedButtons" onclick="changeAltTitle();">Alterar</button></div></div><div class="lightbox box-addOrModifyLink" style="display: none;"><div class="closeLightBox"><p class="closeLightBoxButton" onclick="closeLightBox()">X</p></div><div class="container"><input class="input-addOrModifyLink" placeholder="Insira o link da imagem"></div><div class="container"><button class="fixedButtons" onclick="addOrModifyLink();">Alterar ou criar link</button></div></div></div>' + '</div>');
			$('body').append(lateralBox);

			let lateralBoxCss = $('<style>body{margin: 0;}button{cursor: pointer;}#box{background: rgba(255, 255, 255, 0.95);height: 100vh;position: fixed;right: 0;top: 0;width: 30vw;}#box:nth-child(1){margin-top: 25px;}#box,#box *{box-sizing: border-box;outline: none;}#box .container{clear: both;display: block;width: 100%;}#box .container p{font-family: Tahoma, Verdana, Segoe, sans-serif;color: #333333;display: block;margin: 15px auto;width: 90%;}#box textarea.psCss,#box textarea.psText{height: 100px;}#box textarea, #box select,#box button{border: none;display: block;margin: 15px auto;position: relative;width: 90%;}#box textarea,#box select{border: 1px solid #000000;}#box button:active,.closeLightBoxButton:active{left: 1px;top: 1px;}#box textarea.psBackground,#box select{font-size: 16px;height: 50px;}#box .fixedButtons{background: #b5121b;color: #fff;width: 90%;margin: 20px auto;height: 35px;font-size: 16px;}/* lightbox */.lightboxMaster{display: none;position: fixed;width: 100vw;height: 100vh;background: rgba(0, 0, 0, 0.6);margin: 0;padding: 0;border: 0;}.lightbox{display: none;background: rgba(255, 255, 255, 1);height: auto;padding: 25px;margin: 0 auto;top: 15%;width: 75vw;}.lightbox:nth-child(1){margin-top: 5%;}.lightbox,.lightbox *{box-sizing: border-box;outline: none;}.lightbox .container{clear: both;display: block;width: 100%;}.lightbox .container p{font-family: Tahoma, Verdana, Segoe, sans-serif;color: #333333;display: block;margin: 15px auto;width: 90%;}.lightbox textarea.psCss,.lightbox textarea.psText{height: 100px;}.lightbox textarea, .lightbox select, .lightbox input,.lightbox button{border: none;display: block;margin: 15px auto;position: relative;width: 90%;}.lightbox textarea,.lightbox select, .lightbox input{border: 1px solid #000000;}.lightbox button:active{left: 1px;top: 1px;}.lightbox textarea.psBackground,.lightbox select, .lightbox input{padding-left: 10px;font-size: 16px;height: 50px;}.lightbox input{padding-left: 10px;}.lightbox .fixedButtons{background: #b5121b;color: #fff;width: 90%;margin: 20px auto;height: 35px;font-size: 16px;}.closeLightBox{text-align: right;}.closeLightBoxButton{margin: 0;cursor: pointer;position: relative;display: inline-block;padding: 5px 10px;font-family: Tahoma, Verdana, Segoe, sans-serif;font-size: 20px;color: #fff;background: #b5121b;}</style>')[0];
			head.append(lateralBoxCss);
			// /GET CSS FONTS
		}); //$
	};
}, 400); //jQueryReady



//FUNCTIONS FUNCTIONS FUNCTIONS
Array.prototype.clone = function() {
	return this.slice(0);
};





// LIGHTBOX
function changeAltTitle(){
	if( top.imgChangeLink != undefined ){
		let altTitle = $('.imgAlt').val();
		$('.imgAlt').val('');
		top.imgChangeLink.attr({
			'alt' : altTitle,
			'title' : altTitle,
		});
		if( altTitle.length == 0 ){
			alert('Como o campo está vazio, você removeu os atributos ALT e TITLE.');
		};
		closeLightBox();
		top.imgChangeLink = undefined;
	};
};
function addOrModifyLink(){
	let thisLink = $('.input-addOrModifyLink').val();
	if( thisLink.indexOf('http') == -1 ){
		thisLink = 'http://' + thisLink;
		alert('O link não continha o protocolo HTTP ou HTTP. Foi adicionado o http:// na frente do link');
	};
	let a = '';
	//se não houver link
	if( top.imgAddLink.parent().prop('tagName') != 'A' ){
		//cria o elemento 'A'
		a = createA( thisLink );
		a.append( top.imgAddLink.clone() );
		a.insertAfter( top.imgAddLink );
		top.imgAddLink.remove();
	}else{
		//senão apenas altere o link do 'A' já existente
		a = top.imgAddLink.parent();
		a.attr('href', thisLink);
	};

	//previno o comportamento padrão deste link
	a.on('click', function(event){
		event.preventDefault();
	});
	//retorno o evento nesta imagem
	eventoClick( a.find('img') );
	//zero o valor do input
	$('.input-addOrModifyLink').val('');
	//removo a referência
	top.imgAddLink = undefined;
	//fecho o lightbox
	closeLightBox();
};
// /LIGHTBOX

function closeLightBox(){
	$('.lightboxMaster, .lightbox').css('display', 'none');
};


//calcula percentualmente, via o evento click do mouse, qual será o alinhamento horizontal do span
function span_getHorizontalAlign(event){
	//addEventPercentX
	event.percentX = Number(((event.offsetX * 100) / event.currentTarget.width).toFixed(0));
	let align = '';
	if (event.percentX <= 15) {
		align = 'left';
	};
	if (event.percentX > 15 && event.percentX < 75) {
		align = 'center';
	};
	if (event.percentX >= 75) {
		align = 'right';
	};
	return align;
};

//calcula percentualmente, via o evento click do mouse, qual será o alinhamento vertical do span
function span_getVerticalAlign(event){
	//addEventPercentY
	event.percentY = Number(((event.offsetY * 100) / event.currentTarget.height).toFixed(0));
	let align = '';
	if (event.percentY <= 15) {
		align = 'top';
	};
	if (event.percentY > 15 && event.percentY < 75) {
		align = 'middle';
	};
	if (event.percentY >= 75) {
		align = 'bottom';
	};
	return align;
};





function getBgColor(useWhite = 0){
	let bgColor = $('.psBackground').val();
	if( bgColor[0] == '#' ){
		bgColor = bgColor.substring(1,);
	};
	//if HEX
	if( !!bgColor.match(/[0-9abcdefABCDEF]{3}/g) && ( bgColor.length == 3 || bgColor.length == 6 ) ){
		if( bgColor.length == 3 ){
			bgColor = bgColor + bgColor;
		};
		bgColor = '#' + bgColor;
		return bgColor;
	};
	if( bgColor.length > 0 ){
		return bgColor;
	};
	if( useWhite == 0 ){
		return '#00ff00';
	}else{
		return '#ffffff';
	};
};

function cssBySelection() {
	top.selObj = getSelection();
	if (getSelection().toString() != '') {
		selObj = {
			'afterString': selObj_GetAfterString(selObj),
			'beforeString': selObj_GetBeforeString(selObj),
			'data': selObj_GetData(selObj),
			'parent': selObj_GetParent(selObj),
			'selection': selObj,
			'text': selObj_GetText(selObj),
		};
		if( $('table').eq(0).find( selObj.parent ).length > 0 ){
			let nodesLength = selObj.parent.childNodes.length;
			for (let i = 0; i < nodesLength; i++) {
				let thisNode = selObj.parent.childNodes[i];
				if (thisNode.textContent == selObj.data) {
					let simuleSelectedArea = selObj.data.substring(selObj.selection.anchorOffset, selObj.selection.extentOffset);
					if (simuleSelectedArea == selObj.text) {
						let newElement = $('<span class="css-by-selection-temp">' + selObj.beforeString + getSpanWithCSS(selObj.text)[0].outerHTML + selObj.afterString + '</span>')[0];
						selObj.parent.replaceChild(newElement, thisNode);
						$('.css-by-selection-temp')[0].outerHTML = $('.css-by-selection-temp').html();
						break;
					};
				};
			};
			return;
		};
	};
	alert('Não foi encontrado seleção na tabela.');
};

function selObj_GetAfterString(selObj) {
	let afterString = selObj.anchorNode.data;
	afterString = afterString.substring(selObj.extentOffset, );
	return afterString;
};

function selObj_GetBeforeString(selObj) {
	let beforeString = selObj.anchorNode.data;
	beforeString = beforeString.substring(0, selObj.anchorOffset)
	return beforeString;
};

function selObj_GetData(selObj) {
	return selObj.anchorNode.data;
};

function selObj_GetParent(selObj) {
	return selObj.extentNode.parentNode;
};

function selObj_GetText(selObj) {
	return selObj.toString().trim();
};


// INÍCIO CRIA A SPAN VIA OBJS
function getSpanWithCSS(text){
	let infos = {
		'css' : psCss(),
		'text' : psText(),
	};
	if( text != undefined ){
		infos.text = text;
	};

	if( infos.css != '' && infos.text != '' ){
		//transforma o cssJSON em span com seus valores
		return $('<span style="color: ' + infos.css['color'] + '; font-family: ' + infos.css['font-family'] + '; font-size: ' + infos.css['font-size'] + 'px; font-weight: ' + infos.css['font-weight'] + '; font-style: ' + infos.css['font-style'] + '; line-height: ' + infos.css['line-height'] + ';">' + infos.text + '</span>');
	}else{
		alert('Os campos "CSS" e "Texto" não podem estar vazios.');
	};
};

function psText(){
	let texto = $('.psText').val();
	//htmlEntities, para evitar que o texto conflite com o HTML
	texto = texto
		.replace(/"/g, '&quot;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;')
		.replace(/\'/g, '&apos;');
	//normaliza e converte em HTML as quebras de linha do photoshop
	texto = texto.replace(/(\|\n)/g, '<br>').trim();
	return texto;
};

function psCss(){
	var cssString = $('.psCss').val();

	cssString = cssString.substring( cssString.indexOf('{'), cssString.lastIndexOf('}') + 1);

	//get keys
	var keys = cssString.match(/[a-zA-Z\-]{0,50}\:/g);
	//remove os caracteres desnecessários
	for(var i = 0; i < keys.length; i++){
		keys[i] = keys[i].replace(':', '');
	};
	//console.log(keys);

	//get values
	var values = cssString.match(/: .+?;/g);
	//remove os caracteres desnecessários
	for(var i = 0; i < values.length; i++){
		values[i] = values[i].replace(/^:[ ]/, '').replace(/\;$/, '');
	};
	//console.log(values);

	//converte em JSON
	top.cssJSON = {};

	for(var i = 0; i < keys.length; i++){
		cssJSON[keys[i]] = values[i];
	};
	//console.log(cssJSON);

	//tratamento no JSON
	//se o JSON contém todos os valores obrigatórios
	if( cssJSON_checkEssencial( cssJSON ) == true ){
		//adiciona os valores padrões, quando não existir nos valores opcionais
		//console.log(cssJSON);
		cssJSON = cssJSON_addDefaultValues(cssJSON);
		//console.log(cssJSON);

		//simplifica o valor de font-size
		cssJSON['font-size'] = Number( Number( cssJSON['font-size'].match(/[0-9\.]/g).join('') ).toFixed(0) );

		//se houver distorção da fonte no photoshop (quando houver webkit-transform no cssString)
		if( typeof cssJSON['-webkit-transform'] != 'undefined' ){
			//calcula a distorção da fonte para normalizar o font-size (altura * tamanho da fonte)
			if( !!cssJSON['webkit-transform'].match(/\,[0-9]\.[0-9]{3,20}/g) ){
				//se houver distorção para MAIS
				transform = Number( json['-webkit-transform'].match(/\,[0-9]\.[0-9]{3,20}/g)[0].match(/[0-9\.]/g).join('') );
			}else if( !!cssJSON['-webkit-transform'].match(/\( [0-9]\.[0-9]{3,20}/g) ){
				//se houver distorção para MENOS
				transform = Number( cssJSON['-webkit-transform'].match(/\( [0-9]\.[0-9]{3,20}/g)[0].match(/[0-9\.]/g).join('') );
			};
			if( transform.toString() != 'NaN' ){
				cssJSON['font-size'] = ( cssJSON['font-size'] * transform ).toFixed(0);
			};
		};
		//console.log( cssJSON['font-size'] );



		//FONT-FAMILY
		//por padrão agora todos serão Tahoma. Fontes diferentes devem ser setadas manualmente.
		cssJSON['font-family'] = 'Tahoma, Verdana, Segoe, sans-serif';
		/*
		cssJSON['font-family'] = cssJSON['font-family'] + ', sans-serif';
		//FIX photoshop
		cssJSON['font-family'] = cssJSON['font-family'].replace('ArialNarrow', 'Arial Narrow');
		if( cssJSON['font-family'] == '\'Arial\', sans-serif' ){
			cssJSON['font-family'] = 'Arial Narrow, sans-serif';
		};
		*/
		//console.log( cssJSON['font-family'] );



		//COLOR
		let thisCSS = cssJSON['color'];
		let color = [];
		//separa os valores R, G e B
		thisCSS = thisCSS.match(/[0-9\.]{1,6}[,)]/g);
		for(let i = 0; i < 3; i++){
			thisCSS[i] = thisCSS[i].replace(/[,)]/g, '');
		};
		//if RGBA
		if( typeof color[3] != 'undefined' ){
			alert('ATENÇÃO: O texto possui opacidade. Pegue a cor real do texto usando o conta-gotas no Photoshop.');
			console.warn('ATENÇÃO: O texto possui opacidade. Pegue a cor real do texto usando o conta-gotas no Photoshop.');
		};
		//RGB to HEX
		cssJSON['color'] = '#' + rgbToHex( thisCSS[0], thisCSS[1], thisCSS[2] ).toLowerCase();
		//console.log( cssJSON['color'] );




		//FONT-WEIGHT
		thisCSS = cssJSON['font-weight'];
		if( thisCSS.indexOf('bold') > -1 ){
			thisCSS = '700';
		}else if( thisCSS.indexOf('light') > -1 ){
			thisCSS = '300';
		}else{
			thisCSS = '400';
		};
		cssJSON['font-weight'] = thisCSS;
		//console.log( cssJSON['font-weight'] );



		//FONT-STYLE (ITÁLICO)
		thisCSS = cssJSON['font-style'];
		if( thisCSS.indexOf('italic') > -1 ){
			thisCSS = 'italic';
		}else{
			cssJSON['font-style'] = 'normal';
		};
		//console.log( cssJSON['font-style'] );



		//TEXT-TRANSFORM
		thisCSS = cssJSON['text-transform'];
		if( thisCSS.indexOf('uppercase') > -1 ){
			cssJSON['text-transform'] = 'uppercase';
			//ao invés de jogar no CSS, eu converto o texto em uppercase
			//assim o cliente pode alterar manualmente, se quiser
			infos.text = infos.text.toUpperCase();
		}else{
			cssJSON['text-transform'] = 'none';
		};
		//console.log( cssJSON['text-transform'] );

		return cssJSON;
	}else{
		return false;
	};

};

//checagem se este JSON possui todos os requisitos antes de fazer o tratamento
function cssJSON_checkEssencial( cssJSON ){
	let requiredCSS = ['font-size', 'font-family', 'color'];
	for(let i = 0; i < requiredCSS; i++){
		if( typeof cssJSON[requiredCSS] == 'undefined' ){
			alert('A propriedade "' + requiredCSS + '" é obrigatória');
			console.error('A propriedade "' + requiredCSS + '" é obrigatória');
			return false;
		};
	};
	return true;
};

function cssJSON_addDefaultValues( cssJSON ){
	if( typeof cssJSON['font-weight'] == 'undefined' ){
		cssJSON['font-weight'] = '400';
	};
	if( typeof cssJSON['font-style'] == 'undefined' ){
		cssJSON['font-style'] = 'normal';
	};
	if( typeof cssJSON['text-transform'] == 'undefined' ){
		cssJSON['text-transform'] = 'none';
	};
	//outlook FIX
	cssJSON['line-height'] = 'normal';

	return cssJSON;
};
// FIM CRIA A SPAN VIA OBJS























function getGroupSize(thisRowImage, thisImgGroup) {
	if (thisImgGroup != undefined) {
		//se o getGroupSize receber dois valores
		groupSize = thisImgGroup.clone();
		groupSize.unshift(thisRowImage);
	} else {
		//se o getGroupSize receber um valor
		thisImgGroup = thisRowImage;
		groupSize = thisImgGroup.clone();
	};
	groupSize = {
		x: autoSetWidth(groupSize),
		y: autoSetHeight(groupSize)
	};
	return groupSize;
};
//calcula o tamanho da tabela a partir da posição inicial dos filhos
function autoSetWidth(jsonImgs) {
	if (jsonImgs.constructor == {}.constructor) {
		jsonImgs = [jsonImgs];
	};
	jsonImgs = jsonImgs.clone();
	temp = sortJsonsAB(jsonImgs, 'xOrder');
	xMin = temp[0].x;
	xMax = temp[temp.length - 1].xLast;
	return xMax - xMin; //width
};

function autoSetHeight(jsonImgs) {
	if (jsonImgs.constructor == {}.constructor) {
		jsonImgs = [jsonImgs];
	};
	jsonImgs = jsonImgs.clone();
	temp = sortJsonsAB(jsonImgs, 'yOrder');
	yMin = temp[0].y;
	yMax = temp[temp.length - 1].yLast;
	return yMax - yMin; //height
};

function createTable(width = '100%', height = 'auto') {
	let table = $('<table></table>');
	table.attr({
		'width': width,
		'height': height,
		'valign': 'top',
		'border': '0',
		'cellpadding': '0',
		'cellspacing': '0',
		'style': 'font-size: 0;'
	});
	table.append(createTbody(width, height));
	return table;
};

function createTbody(width = '100%', height = 'auto') {
	let tbody = $('<tbody></tbody>');
	tbody.attr({
		//'width': width,
		//'height': height,
		'valign': 'top',
		'border': '0',
		'style': 'font-size: 0;'
	});
	return tbody;
};

function createTr(width = '100%', height = 'auto') {
	let tr = $('<tr></tr>');
	tr.attr({
		//'width': width,
		//'height': height,
		'valign': 'top',
		'style': 'font-size: 0;'
	});
	return tr;
}

function createTd(width = '100%', height = 'auto') {
	let td = $('<td></td>');
	td.attr({
		'width': width,
		'height': height,
		'valign': 'top',
		'style': 'font-size: 0;'
	});
	return td;
};

function createImg(jsonImg) {
	let img = $('<img>');
	img.attr({
		src: jsonImg.src,
		width: jsonImg.width,
		height: jsonImg.height,
		'border': '0',
		'style': 'display: block; border: 0;'
	});

	if( typeof jsonImg.alt != 'undefined' ){
		img.attr({
			'alt': jsonImg.alt,
			'title': jsonImg.alt
		});
	};

	if( typeof jsonImg.ahref != 'undefined' ){
		img.attr({
			'alt': jsonImg.alt,
			'title': jsonImg.alt
		});
		let a = createA(jsonImg.ahref);
		return a.append(img);
	};
	return img;
};

function createA( link ){
	let a = $('<a href="' + link + '"></a>');
	a.attr({
		target : '_blank',
		'border': '0',
		'style': 'text-decoration: none; display: block; border: 0; font-size: 0; cursor: pointer;'
	});
	return a;
};

function jsonImgsFindAndSetUndefined(json, jsonToRemove) {
	json = json.clone();
	jsonToRemove = jsonToRemove.clone();
	//CONVERT IN ARRAY
	if (jsonToRemove.constructor == {}.constructor) {
		jsonToRemove = [jsonToRemove];
	};
	//json to string
	for (i = 0; i < json.length; i++) {
		json[i] = JSON.stringify(json[i]);
	};
	//jsonToRemove to string
	for (i = 0; i < jsonToRemove.length; i++) {
		jsonToRemove[i] = JSON.stringify(jsonToRemove[i]);
	};
	//compare and remove
	for (i = 0; i < jsonToRemove.length; i++) {
		while (json.indexOf(jsonToRemove[i]) != -1) {
			json[json.indexOf(jsonToRemove[i])] = undefined;
		};
	};
	//reconvert string in JSONs itens
	for (i = 0; i < json.length; i++) {
		if (json[i] != undefined) {
			json[i] = JSON.parse(json[i]);
		};
	};
	return json;
};

//gera o JSON principal, com todas as informações
function imgsToJSON() {
	let json = [];
	$('img').not('[src="images/spacer.gif"]').each(function() {
		let img = {
			'src': $(this).attr('src'),
			'width': $(this)[0].naturalWidth,
			'height': $(this)[0].naturalHeight,
			'xOrder': getTotal(Number($(this).position().left.toFixed(0)), Number($(this).position().top.toFixed(0))),
			'yOrder': getTotal(Number($(this).position().top.toFixed(0)), Number($(this).position().left.toFixed(0))),
			'x': Number($(this).position().left.toFixed(0)),
			'y': Number($(this).position().top.toFixed(0)),
			'xLast': (Number($(this).position().left.toFixed(0)) + $(this)[0].naturalWidth),
			'yLast': (Number($(this).position().top.toFixed(0)) + $(this)[0].naturalHeight),
			'alt': $(this).attr('alt'),
			'title': $(this).attr('alt'),
			'ahref': $(this).attr('ahref'),
		};
		json.push(img);

		//define o ID sem usar o nome do arquivo
		json = sortJsonsAB(json, 'yOrder');
		for(let i = 0; i < json.length; i++){
			let thisImg = json[i].id = i + 1;
		};

	});
	return json;
};
//soma dos valores xOrder e yOrder, permitindo ordenar o json a partir delas
function getTotal(a, b) {
	a = a.toString();
	while (a.length < 5) {
		a = '0' + a;
	};
	b = b.toString();
	while (b.length < 5) {
		b = '0' + b;
	};
	return (a + b);
};
//ordena o JSON a partir dos valores da propriedade setada
function sortJsonsAB(arrayWithJsons, itemToSort) {
	//clona o array json
	arrayWithJsons = arrayWithJsons.clone();
	arrayWithJsons.sort(function(a, b) {
		return a[itemToSort] - b[itemToSort];
	});
	return arrayWithJsons;
};






function salvar(){
	let newHTML = $('html').clone();
	newHTML.find('table').eq(0).attr({
		'style': newHTML.find('table').eq(0).attr('style') + ' margin: 0 auto;'
	});
	$(newHTML, newHTML.find('body')).attr({
		'style': 'width: 100%; margin: 0 auto;'
	});

	//reset head
	newHTML.find('title').html('email');
	newHTML.find('meta').remove();
	$('<meta charset="utf-8" />').prependTo( newHTML.find('head') );
	newHTML.find('style, script').remove();
	newHTML.find('body > *').not('table').remove();

	let nomeDoEmail = location.href;
	nomeDoEmail = nomeDoEmail.substring(nomeDoEmail.lastIndexOf('/') + 1, nomeDoEmail.lastIndexOf('.'));

	let doctype = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">' + '\n';
	newHTML.find('[idforbackup]').removeAttr('idforbackup');

	//adiciona comentário para o MSO 12 não dar erro
	let commentMSO12 = '<!--[if gte mso 12]><style type="text/css">body,table,td{font-family:Tahoma, Verdana, Segoe, sans-serif !important;text-decoration:none}</style><![endif]-->';
	$( commentMSO12 ).insertBefore( newHTML.find('body') );
	$( commentMSO12 ).insertBefore( newHTML.find('body').children().eq(0) );

	SalvarComo(nomeDoEmail, '.html', doctype + newHTML[0].outerHTML);
	for( let i = 0; i < newHTML.find('img').length; i++ ){
		// não remover o setTimeout, isso impede o erro no MIME TYPE visto no console do chrome
		setTimeout(function(){
			download( newHTML.find('img').eq(i).attr('src') );
		},(100 * i));
	};
};

function removeHeights(){
	// isso serve para tornar todos os blocos com tamanhos dinâmicos
	var Trs = $('table').eq(0).children().children();
	Trs.each(function(){
		// se for espaço vazio
		if( $(this).text().trim().length == 0 ){
			// então remove a cascata de [height], mantendo apenas height no último elemento
			var thisTrheights = $(this).find('[height]');
			thisTrheights.not( thisTrheights.last() ).removeAttr('height');
		}else{
			// se não, basta tirar todos os heights de tudo, exceto imagens pra garantir
			$(this).find('[height]').not('img').removeAttr('height');
		};
	});
};


//CTRL + Z e CTRL + Y
function monitoraTableMaster(){
	setInterval(function(){
		let tableMaster = $('table').eq(0);
		if( tableBackup.lastIndexOf( tableMaster[0].outerHTML ) == -1 ){
			let backupIdForBackup = tableMaster.attr('idforbackup');

			let newTableBackup = [];
			for( let i = 0; i < tableBackup.length; i++ ){
				if( tableBackup[i].indexOf(backupIdForBackup) == -1 ){
					newTableBackup.push( tableBackup[i] );
				}else{
					newTableBackup.push( tableBackup[i] );
					break;
				};
			};
			tableMaster.attr('idforbackup', randomString());
			newTableBackup.push( tableMaster[0].outerHTML );
			tableBackup = newTableBackup;
		}else{
			//se o histórico estiver muito grande, remove os mais antigos...
			if( tableBackup.length > 40 ){
				tableBackup.splice(0, tableBackup.length - 40);
			};
		};
	}, 50);
};



//CTRL + Z e CTRL + Y
function ctrlZ(){
	document.onkeydown = function(event){
		//if CTRL + Z
		if( event.ctrlKey && event.key == 'z' ){
			event.preventDefault();
			//desfaz
			let tableMaster = $('table').eq(0);
			let tableMasterHTML = tableMaster[0].outerHTML;

			let newTable = tableBackup[ tableActive( 'y', tableMasterHTML ) - 1 ];
			if( typeof newTable != 'undefined' ){
				if( newTable.toString() != 'NaN' ){
					tableMaster[0].outerHTML = newTable;
					eventoClick($('table').eq(0).find('img'));
					preventA( $('table').eq(0).find('a') );
				};
			};
		};
		//if CTRL + Y
		if( event.ctrlKey && event.key == 'y' ){
			event.preventDefault();
			//refaz
			let tableMaster = $('table').eq(0);
			let tableMasterHTML = tableMaster[0].outerHTML;

			let newTable = tableBackup[ tableActive( 'y', tableMasterHTML ) + 1 ];
			if( typeof newTable != 'undefined' ){
				if( newTable.toString() != 'NaN' ){
					tableMaster[0].outerHTML = newTable;
					eventoClick($('table').eq(0).find('img'));
					preventA( $('table').eq(0).find('a') );
				};
			};
		};
	};
};

function tableActive( action, tableMasterHTML ){
	if( tableBackup.length != 0 ){
		let timelineBackupPosition = tableBackup.lastIndexOf( tableMasterHTML );
		if( timelineBackupPosition > -1 ){
			return timelineBackupPosition;
		}else{
			if(action == 'z'){
				//if Z
				return tableBackup.length - 1;
			}else{
				//if Y
				return NaN;
			};
		};
	}else{
		return NaN;
	};
};





//JS SalvarComo, criado por Luis Lobo
function SalvarComo(o,e,t){for("."!=(e=e.toLowerCase()).substring(0,1)&&(e="."+e),SCRemovidos=!1;0==SCRemovidos;)"\t"==t.substring(0,1)||" "==t.substring(0,1)||"\n"==t.substring(0,1)?(t=t.substring(1,t.length),ok1=!1):ok1=!0,"\t"==t.substring(t.length-1,t.length)||" "==t.substring(t.length-1,t.length)||"\n"==t.substring(t.length-1,t.length)?(t=t.substring(0,t.length-1),ok2=!1):ok2=!0,1==ok1&&1==ok2&&(SCRemovidos=!0);if("csv"==e||".csv"==e){for(t=t.split("\n"),SCConteudoCompleto="";t.length>0;){SCConteudoTemp=t[0],t.splice(0,1),SCConteudoTemp=SCConteudoTemp.split("\t");for(var n=0;n<SCConteudoTemp.length;n++)SCConteudoTemp[n].indexOf('"')>-1||SCConteudoTemp[n].indexOf(";")>-1?n!=SCConteudoTemp.length-1?SCConteudoCompleto+='"'+SCConteudoTemp[n].replace(/"/g,'""')+'";':SCConteudoCompleto+='"'+SCConteudoTemp[n].replace(/"/g,'""')+'"':n!=SCConteudoTemp.length-1?SCConteudoCompleto+=SCConteudoTemp[n]+";":SCConteudoCompleto+=SCConteudoTemp[n];SCConteudoCompleto+="\n"}t=SCConteudoCompleto.substring(0,SCConteudoCompleto.length-1);C=new Blob([t],{type:"text/plain;charset=utf-8"});saveAs(C,o+e)}if("txt"==e||".txt"==e||".html"==e){var C=new Blob([t],{type:"text/plain;charset=utf-8"});saveAs(C,o+e)}}

//JS download, criado por Luis Lobo
function download(e,o,n){if(0!=o&&0!=o&&null!=o||(o=void 0),"object"==typeof e&&0==$.isArray(e)&&e.each(function(){void 0==n?void 0!=$(this).attr("src")?download(e=$(this).attr("src"),o):void 0!=$(this).attr("href")?download(e=$(this).attr("href"),o):console.error("The item "+$(this)+'can not be downloaded. There is no "src" or "href" attribute.'):void 0!=$(this).attr(n)?download(e=$(this).attr(n),o):console.error("The item "+$(this)+'can not be downloaded. There is no "'+n+'" attribute.')}),1==$.isArray(e))for(i=0;i<e.length;)download(e[i],o),i++;else"string"==typeof e&&""!=e&&"#"!=e?(void 0!=o&&e.indexOf("http")>-1&&(linkHostname=e,linkHostname=linkHostname.substring(linkHostname.indexOf("//")+2,linkHostname.length),linkHostname=linkHostname.substring(0,linkHostname.indexOf("/")),linkHostname!=location.hostname&&console.error("Files on other sites can not be renamed. Your file was saved with the original name.")),void 0==o&&(o=-1==e.indexOf("/")?e:e.substring(e.lastIndexOf("/")+1,e.length)),o=decodeURIComponent(o),o=o.replace(/\\/g,"").replace(/\//g,"").replace(/\:/g,"").replace(/\*/g,"").replace(/\?/g,"").replace(/\"/g,"").replace(/\</g,"").replace(/\>/g,"").replace(/\|/g,""),btnDownload=$('<a href="'+e+'" download="'+o+'" ></a>'),btnDownload[0].click()):console.error("This item can not be downloaded because it does not have a valid link.")}

//PARTE ESPECIFICA DO CLIQUE QUE ADICIONA A SPAN
function clickToPsCss( cssString, texto ){
	if( $('.psText').val().length > 0 ){
		//corrige o caractere inválido do photoshop, que deveria representar a quebra de linha
		texto = $('.psText').val().replace(/\/g, '\n').trim();

		//escapa os caracteres que poderiam conflitar com o HTML
		texto = texto
			.replace( /"/g, '&quot;' )
			.replace( /'/g, '&#39;' )
			.replace( /</g, '&lt;' )
			.replace( />/g, '&gt;' );
	}else{
		//se não houver insere o texto de exemplo
		texto = 'TEXTO';
	};
	//seta no span via .html(), substituindo as quebras de linha por tag br
	console.log(texto);
	console.log(texto.replace(/\n/g, '<br>'));
	let span = $('<span><span>').html(texto.replace(/\n/g, '<br>'));

	//converte as informações do photoshop em objeto para trata-las
	cssString = cssString.substring( cssString.indexOf('{'), cssString.lastIndexOf('}') + 1 );
	//get propriedades
	propriedades = cssString.match(/[a-zA-Z-]{0,50}\:/g);
	//limpa caracteres desnecessários
	for(i = 0; i < propriedades.length; i++){
		propriedades[i] = propriedades[i].replace(/\:/g, '');
	};
	console.log(propriedades);

	//get valores
	valores = cssString.match(/: .+?;/g);
	//limpa caracteres desnecessários
	for(i = 0; i < valores.length; i++){
		valores[i] = valores[i].replace(/\^:[ ]/g, '').replace(/\;$/g, '');
	};
	console.log(valores);

	//transforma em objeto JSON agrupando propriedade : valor
	json = {};
	for(i = 0; i < propriedades.length; i++){
		json[propriedades[i]] = valores[i];
	};
	console.log(json);


	//com as informações já no JSON, trate os valores
	//FONT-SIZE
	if( typeof json['font-size'] != 'undefined' ){
		//simplifica o valor e converte em número
		fontSize = Number( Number( json['font-size'].match(/[0-9\.]/g).join('') ).toFixed(0) );
		//se houver distorção da fonte no photoshop (quando aumentam a fonte pelo box no photoshop)
		if( typeof json['-webkit-transform'] != 'undefined' ){ //se ele existe
			if( !!json['-webkit-transform'].match(/\,[0-9]\.[0-9]{3,20}/g) ){ //se houver distorção para MAIS
				//calcule o valor da distorção da altura vezes o tamanho da fonte, assim encontramos o tamanho real
				transform = Number( json['-webkit-transform'].match(/\,[0-9]\.[0-9]{3,20}/g)[0].match(/[0-9\.]/g).join('') );
			}else if( !!json['-webkit-transform'].match(/\( [0-9]\.[0-9]{3,20}/g) ){ //se houver distorção para MENOS
				//calcule o valor da distorção da altura vezes o tamanho da fonte, assim encontramos o tamanho real
				transform = Number( json['-webkit-transform'].match(/\( [0-9]\.[0-9]{3,20}/g)[0].match(/[0-9\.]/g).join('') );
			};
			if( transform.toString() != 'NaN' ){
				fontSize = fontSize * transform;
				fontSize = fontSize.toFixed(0);
			};
		};
		fontSize = fontSize + 'px';
		console.log(fontSize);
	}else{
		console.error('ERRO: A PROPRIEDADE \'FONT-SIZE\' É OBRIGATÓRIA');
		alert('ERRO: A PROPRIEDADE \'FONT-SIZE\' É OBRIGATÓRIA');
		return false;
	};

	//FONT-FAMILY
	//por padrão agora é sempre Tahoma, para alterar é preciso fazer manualmente
	json['font-family'] = 'Tahoma, Verdana, Segoe, sans-serif';
	fontFamily = json['font-family'];

	//COLOR
	if( typeof json['color'] != 'undefined' ){
		//separe os valores R, G e B do grupo
		color = json['color'].match(/[0-9\.]{1,6}[,)]/g);
		//R em RGB
		color[0] = color[0].replace(',', '');
		//G em RGB
		color[1] = color[1].replace(',', '');
		//B em RGB
		color[2] = color[2].replace(')', '');
		//SE FOR RGBA
		if( typeof color[3] != 'undefined' ){
			console.warn('ATENÇÃO: O texto possui opacidade. Pegue a cor real do texto usando o conta-gotas no photoshop.');
			alert('ATENÇÃO: O texto possui opacidade. Pegue a cor real do texto usando o conta-gotas no photoshop.');
		};

		//RGB agora se transforma em HEX
		color = '#' + rgbToHex(color[0], color[1], color[2]).toLowerCase();
		console.log(color);
	}else{
		console.error('ERRO: A PROPRIEDADE \'COLOR\' É OBRIGATÓRIA');
		alert('ERRO: A PROPRIEDADE \'COLOR\' É OBRIGATÓRIA');
		return false;
	};

	//FONT-WEIGHT
	if( typeof json['font-weight'] != 'undefined' ){
		//se houver valor de font-weight, insira no código
		fontWeight = json['font-weight'];
		if( fontWeight.indexOf('bold') > -1 ){
			fontWeight = '700';
		}else if( fontWeight.indexOf('light') > -1 ){
			fontWeight = '300'
		}else{
			fontWeight = '400';
		};
	}else{
		//senão, adicione o valor padrão
		fontWeight = '400';
	};
	console.log(fontWeight);

	//FONT-STYLE (ITÁLICO)
	if( typeof json['font-style'] != 'undefined' ){
		//se houver valor de font-style, insira no código
		fontStyle = json['font-style'];
		if( fontStyle.indexOf('italic') > -1 ){
			fontStyle = 'italic';
		}else{
			fontStyle = 'normal';
		};
	}else{
		//senão, adicione o valor padrão
		fontStyle = 'normal';
	};
	console.log(fontStyle);

	lineHeight = 'normal';
	//setado com o valor padrão pois o outlook não aceita este valor

	//TEXT-TRANSFORM
	if( typeof json['text-transform'] != 'undefined' ){
		//se houver valor de line-height, insira no código
		textTransform = json['text-transform'].replace(': ', '');
		if( textTransform.indexOf('uppercase') > -1 ){
			textTransform = 'uppercase';
			//AO INVÉS DE JOGAR ESTA PROPRIEDADE NO STYLE, EU CONVERTO A FONTE EM UPPERCASE
			//assim o cliente pode alterar por lá caso queria
			span.html( span.html().toUpperCase() );

		};
	}else{
		//senão adicione o valor padrão
		textTransform = 'none';
	};
	console.log(textTransform);

	//joga todos os valores no SPAN
	style =
	'color: ' + color + '; ' +
	'font-family: ' + fontFamily + '; ' +
	'font-size: ' + fontSize + '; ' +
	'font-weight: ' + fontWeight + '; ' +
	'font-style: ' + fontStyle + '; ' +
	'line-height: ' + lineHeight + ';';

	span.attr({
		'style': style
	});

	return span;
};

//fonte http://www.javascripter.net/faq/index.htm
function rgbToHex(t,e,n){return toHex(t)+toHex(e)+toHex(n)}function toHex(t){return t=parseInt(t,10),isNaN(t)?"00":(t=Math.max(0,Math.min(t,255)),"0123456789ABCDEF".charAt((t-t%16)/16)+"0123456789ABCDEF".charAt(t%16))}